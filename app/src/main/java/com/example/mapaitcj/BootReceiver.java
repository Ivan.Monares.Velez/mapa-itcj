package com.example.mapaitcj;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = "ApplicationBootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            boolean isOn = prefs.getBoolean(LessonAlarms.PREF_IS_ALARM_ON, false);
            LessonAlarms.setAlarms(context, isOn);
            Log.i(TAG, "set alarm from boot");
        }
    }
}
