package com.example.mapaitcj;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import java.util.ArrayList;

public class MapFragment extends Fragment {

    private final static String TAG = "MapFragment";
    private final static String EXTRA_LOCATION_ID = "com.example.mapaicj.location_id";

    public static final String PREFS_FILE = "maptracking";
    public static final String PREF_HAS_PERMISSIONS = "maptracking.permissions";
    private final static String PREF_TRACKING = "maptracking.permission";

    public static final double MIN_LAT = 31.717000; // Right 31.716575
    public static final double MAX_LAT = 31.721875; // Left 31.721917

    public static final double MIN_LON = -106.424800; // Bottom -106.424683
    public static final double MAX_LON = -106.418600; // Top -106.418576

    private Location mParent;
    private Location mLocation;

    private MapView mMapView;
    private Spinner mSiblingsSpinner;
    private TextView mTitle;
    private FrameLayout mFrameLayout;
    private View route;
    private ImageView mLocationTag;
    private ImageView mPositionTag;
    private ImageButton mMyLocationButton;
    private Button exitBuildingButton;
    private Button exploreBuildingButton;
    private RadioGroup mRadioGroup;
    private RadioButton mRadioButton0;
    private RadioButton mRadioButton1;
    private RadioButton mRadioButton2;

    private SharedPreferences mPrefs;
    private boolean isTracking;

    private int check = 0;
    private Matrix matrix = new Matrix();
    private double[] mCoords = {900, 500, 500, 500};

    private double mTrackingLatitude;
    private double mTrackingLongitude;

    private int globalFrameWidth;
    private int globalFrameHeight;
    private double screenCompensationFactor = -1;

    private LocationManager mLocationManager = null;

    private BroadcastReceiver mOnUpdateInterface = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateUI();
        }
    };

    public static MapFragment newInstance(FragmentActivity activity, int id) {

        Bundle args = new Bundle();
        args.putInt(EXTRA_LOCATION_ID, id);

        MapFragment fragment = new MapFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        int id = getArguments().getInt(EXTRA_LOCATION_ID);

        RelativeLayout ui_container = v.findViewById(R.id.ui_container);

        BitmapFactory.Options dimensions = new BitmapFactory.Options();
        dimensions.inJustDecodeBounds = true;
        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.conjunto, dimensions);
        int height = dimensions.outHeight;
        int width =  dimensions.outWidth;


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        globalFrameWidth = size.x - dpToPx(16);
        globalFrameHeight = height * globalFrameWidth / width + dpToPx(32);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(globalFrameWidth, globalFrameHeight);
        layoutParams.setMargins(dpToPx(8), 0, dpToPx(8), 0);
        ui_container.setLayoutParams(layoutParams);

        mTitle = v.findViewById(R.id.map_title);
        mFrameLayout = v.findViewById(R.id.mapFrame);
        mMapView = MainActivity.getMapView();
        try {
            ((FrameLayout) mMapView.getParent()).removeView(mMapView);
        } catch (Exception e) {
            //
        }

        ViewGroup.LayoutParams tagParams = new ViewGroup.LayoutParams(dpToPx(48), dpToPx(48));
        mLocationTag = new ImageView(getContext());
        mLocationTag.setImageResource(R.drawable.ic_target);
        mLocationTag.setLayoutParams(tagParams);

        tagParams = new ViewGroup.LayoutParams(dpToPx(24), dpToPx(24));
        mPositionTag = new ImageView(getContext());
        mPositionTag.setImageResource(R.drawable.ic_position_disabled);
        mPositionTag.setLayoutParams(tagParams);

        //route = new View(getContext());
        //route.setVisibility(View.GONE);

        mSiblingsSpinner = v.findViewById(R.id.siblings_spinner);

        mPrefs = getContext().getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        isTracking = mPrefs.getBoolean(MapFragment.PREF_TRACKING, false);

        mMyLocationButton = v.findViewById(R.id.myLocationButton);
        mMyLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTracking(!isTracking);
            }
        });

        mFrameLayout.addView(mMapView);
        //mFrameLayout.addView(route);
        mFrameLayout.addView(mLocationTag);
        mFrameLayout.addView(mPositionTag);

        mCoords[2] = dpToPx(198);
        mCoords[3] = dpToPx(214);

        exitBuildingButton = v.findViewById(R.id.exitBuildingButton);
        exitBuildingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupView(mParent.getId());
            }
        });

        exploreBuildingButton = v.findViewById(R.id.exploreBuildingButton);
        exploreBuildingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupView(mLocation.getChildrenIds().get(0));
            }
        });

        mRadioGroup = v.findViewById(R.id.radioGroup);
        mRadioButton0 = v.findViewById(R.id.radio0);
        mRadioButton0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    mMapView.setImageResource(R.drawable.conjunto);
                }
            }
        });
        mRadioButton1 = v.findViewById(R.id.radio1);
        mRadioButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    mMapView.setImageResource(R.drawable.conjunto_1);
                }
            }
        });
        mRadioButton2 = v.findViewById(R.id.radio2);
        mRadioButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    mMapView.setImageResource(R.drawable.conjunto_2);
                }
            }
        });


        setupView(id);
        setTracking(isTracking);
        return v;
    }

    public void setupView(int locationId){

        mLocation = LocationTree.get(getContext()).findById(locationId);
        mParent = LocationTree.get(getContext()).findById(mLocation.getParentId());

        mTitle.setText(mLocation.getTitle());

        mCoords[0] = (double) mapToLimits(mLocation.getCoordX(), MAX_LAT, MIN_LAT);
        mCoords[1] = (double) mapToLimits(mLocation.getCoordY(), MAX_LON, MIN_LON);
        mMapView.setZoom(mLocation.getZoom(), (float)mCoords[0], (float)mCoords[1]);

        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("Elija una opción...");
        stringList.addAll(mParent.getChildrenNames());
        ArrayAdapter<String> adapter = new OptionsAdapter(stringList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        check = 0;
        mSiblingsSpinner.setAdapter(adapter);

        int selection = stringList.indexOf(mLocation.getTitle());
        if(selection != -1){
            mSiblingsSpinner.setSelection(selection);
        }

        mSiblingsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()  {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(check++ > 0) {
                    setupView(mParent.getChildrenIds().get(position-1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if(mLocation.getId() == ( LocationTree.get(getContext()).getRoot().getId() )){
            mLocationTag.setVisibility(View.GONE);
            exitBuildingButton.setVisibility(View.INVISIBLE);
        } else{
            mLocationTag.setVisibility(View.VISIBLE);
            exitBuildingButton.setVisibility(View.VISIBLE);
        }

        if(mLocation.getChildCount() > 0 && mSiblingsSpinner.getSelectedItemPosition() != 0){
            exploreBuildingButton.setVisibility(View.VISIBLE);
        } else {
            exploreBuildingButton.setVisibility(View.INVISIBLE);
        }

        if(mLocation.getLevelZ() == 0){
            mRadioButton0.performClick();
        }
        if(mLocation.getLevelZ() == 1){
            mRadioButton1.performClick();
        }
        if(mLocation.getLevelZ() == 2){
            mRadioButton2.performClick();
        }

        updateUI();
    }


    public void setTracking(boolean tracking) {
        isTracking = tracking;

        if (isTracking) {

            boolean permission = mPrefs.getBoolean(MapFragment.PREF_HAS_PERMISSIONS, false);
            if (!permission){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ((AppCompatActivity) getActivity()).requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.MY_PERMISSIONS_REQUEST);
                }
            }

            mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            if(permission && isLocationEnabled()){
                mMyLocationButton.setImageResource(R.drawable.ic_my_location_blue);
                startLocationUpdates();
                isTracking = true;
            }else{
                isTracking = false;
            }
        }

        if(!isTracking){
            mMyLocationButton.setImageResource(R.drawable.ic_my_location_grey);
            mPositionTag.setVisibility(View.GONE);
            if(mLocationManager != null) {
                mLocationManager.removeUpdates(locationListenerGPS);
                mLocationManager = null;
            }
        }

        mPrefs.edit().putBoolean(MapFragment.PREF_TRACKING, isTracking).commit();
    }

    public void startLocationUpdates() {

        boolean permissions = mPrefs.getBoolean(PREF_HAS_PERMISSIONS, false);
        mPositionTag.setVisibility(View.VISIBLE);

        if (!permissions
                && ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        // Get the last known location and broadcast it if you have one
        android.location.Location lastKnown = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (lastKnown != null) {
            setTrackingTag(lastKnown, false);
            mPositionTag.setImageResource(R.drawable.ic_position_disabled);
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGPS);
    }

    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            setTrackingTag(location, true);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {
            setTracking(isTracking);
        }

        @Override
        public void onProviderDisabled(String provider) {
            mPositionTag.setImageResource(R.drawable.ic_position_disabled);
            mLocationManager.removeUpdates(locationListenerGPS);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setMessage("Ha desactivado la ubicación de dispositivo. Por favor actívela para poder rastrar su ubicación.");
            alertDialog.setPositiveButton("Configuraciones de ubicación", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("Desactivar rastreo", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    setTracking(false);
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
    };

    private void setTrackingTag(android.location.Location location, boolean fromGPS){

        mPositionTag.setImageResource(R.drawable.ic_position);
        mTrackingLatitude = location.getLatitude();
        mTrackingLongitude = location.getLongitude();
        String msg="New Latitude: " + mTrackingLatitude + "\nNew Longitude: " + mTrackingLongitude;
        Toast.makeText(getActivity(),msg,Toast.LENGTH_LONG).show();

        mCoords[2] = (double) mapToLimits(mTrackingLatitude, MAX_LAT, MIN_LAT);
        mCoords[3] = (double) mapToLimits(mTrackingLongitude, MAX_LON, MIN_LON);
        mMapView.setZoom(mMapView.getCurrentZoom(), (float)mCoords[2], (float)mCoords[3]);

        mPositionTag.setVisibility(View.VISIBLE);

        //if outside the campus
        if(fromGPS &&  (mTrackingLatitude < MIN_LAT || mTrackingLatitude > MAX_LAT  || mTrackingLongitude < MIN_LON || mTrackingLongitude > MAX_LON) )
        {
            //disable tracking and notify user
            mPositionTag.setVisibility(View.GONE);
            setTracking(false);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setMessage("Su GPS indica que no se encuentra dentro del ITCJ. Por favor ingrese al campus para poder mostrar su posición en el mapa.");
            alertDialog.setPositiveButton(android.R.string.ok, null);
            AlertDialog alert = alertDialog.create();
            alert.show();
        }else {
            // set tag position and updateUI
            updateUI();
        }
    }

    private boolean isLocationEnabled() {

        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setMessage("La ubicación del dispositivo está desactivada. Por favor activela en el menú de configuraciones.");
            alertDialog.setPositiveButton("Configuraciones de ubicación", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton(android.R.string.cancel, null);
            AlertDialog alert = alertDialog.create();
            alert.show();
            return false;
        }
        return true;
    }

    public void updateUI() {

        if(screenCompensationFactor == -1){
            try {
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                Log.i(TAG+"2", "Screen Width:"+size.x );
                Log.i(TAG+"2", "Screen Height:"+size.y );
                double value = ((double)size.x /(double) size.y);
                screenCompensationFactor = (-214.26*value*value + 231.46*value - 61.259);
                Log.i(TAG+"2", "Compensation:"+screenCompensationFactor );
            } catch (Exception e){
                Log.e(TAG+"2", "Error", e);
                screenCompensationFactor = -1;
            }
        }

        //if(trackingActive) updateZoom();
        matrix = mMapView.getImageMatrix();

        float[] mapped = new float[mCoords.length];
        for (int i = 0 ; i < mCoords.length; i++)
        {
            mapped[i] = (float) (mCoords[i] * 1.275);// * 1.142857142857143
            if(i%2 == 0){
                mapped[i] *= globalFrameWidth;
            } else {
                mapped[i] *= globalFrameHeight;
            }
        }

        matrix.mapPoints(mapped);

        Log.i(TAG, "mCoords[0]: " + mCoords[0]);
        Log.i(TAG, "mCoords[1]: " + mCoords[1]);
        Log.i(TAG, "mapped[0]: " + mapped[0]);
        Log.i(TAG, "mapped[1]: " + mapped[1]);

        mLocationTag.setX(mapped[0] - mLocationTag.getMeasuredWidth()/2);
        mLocationTag.setY(mapped[1] - mLocationTag.getMeasuredHeight());

        mPositionTag.setX(mapped[2] - mPositionTag.getMeasuredWidth()/2);
        mPositionTag.setY(mapped[3] - mPositionTag.getMeasuredHeight()/2);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(MapView.UPDATE_INTERFACE);
        getActivity().registerReceiver(mOnUpdateInterface, filter, MapView.PERM_PRIVATE, null);
        setTracking(isTracking);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mOnUpdateInterface);

        if(mLocationManager!= null) {
            mLocationManager.removeUpdates(locationListenerGPS);
            mLocationManager = null;
        }
    }

    public int dpToPx(double dpValue) {
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return  (int) ( dpValue * ((double)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT) );
    }

    double mapToLimits(double x, double in_min, double in_max) {
        double value =  (x - in_min)/ (in_max - in_min);

        if(value < 0) return 0;
        if(value > 1) return 1;

        return value;
    }

    private class OptionsAdapter extends ArrayAdapter<String> {
        public OptionsAdapter(ArrayList<String> options) {
            super(getActivity(), android.R.layout.simple_spinner_item, options);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }
    }
}
