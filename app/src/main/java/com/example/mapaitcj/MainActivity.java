package com.example.mapaitcj;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, HomeFragment.FragmentChanger {

    public final static String TAG = "MapITCJActivity";

    public static final int MY_PERMISSIONS_REQUEST = 0;

    public final static String HOME_FRAGMENT_TAG = "HomeFragment";
    public final static String FAQ_FRAGMENT_TAG = "FaqFragment";
    public final static String MAP_FRAGMENT_TAG = "MapFragment";
    public final static String SCHEDULE_FRAGMENT_TAG = "ScheduleFragment";
    public final static String SEARCH_FRAGMENT_TAG = "SearchFragment";

    public final static String EXTRA_MAP_ID = "Selected_map_id";

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private static MapView mapView;

    public static MapView getMapView(){
        return mapView;
    }

    private BroadcastReceiver mOnShowNotification = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setResultCode(Activity.RESULT_CANCELED);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(LessonAlarms.ACTION_SHOW_NOTIFICATION);
        getApplicationContext().registerReceiver(mOnShowNotification, filter, LessonAlarms.PERM_PRIVATE, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApplicationContext().unregisterReceiver(mOnShowNotification);
    }

    @Override
    public void changeFragment(int id){
        switch (id){
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HomeFragment(), HOME_FRAGMENT_TAG).commit();
                navigationView.setCheckedItem(R.id.nav_home);
                break;
            case R.id.nav_faq:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FaqFragment(), FAQ_FRAGMENT_TAG).commit();
                navigationView.setCheckedItem(R.id.nav_faq);
                break;
            case R.id.nav_map:

                LocationTree lt = LocationTree.get(getApplicationContext());
                int locationId = lt.getRoot().getId();

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        MapFragment.newInstance(this, locationId), MAP_FRAGMENT_TAG).commit();
                navigationView.setCheckedItem(R.id.nav_map);
                break;
            case R.id.nav_schedule:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ScheduleFragment(), SCHEDULE_FRAGMENT_TAG).commit();
                navigationView.setCheckedItem(R.id.nav_schedule);
                break;
            case R.id.nav_search:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new SearchFragment(), SEARCH_FRAGMENT_TAG).commit();
                navigationView.setCheckedItem(R.id.nav_search);
                break;
            case R.id.about:
                Log.i(TAG, "Got here");
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getResources().getString(R.string.about));
                builder.setMessage(getResources().getString(R.string.about_message));
                builder.setPositiveButton(android.R.string.ok, null);
                Log.i(TAG, "Got here2");
                AlertDialog alert = builder.create();
                Log.i(TAG, "Got here3");
                alert.show();
                Log.i(TAG, "Got here4");
                break;
            default:
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.i(TAG, "New intent");

        super.onNewIntent(intent);
        setIntent(intent);//must store the new intent unless getIntent() will return the old one
        processExtraData();
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i(TAG, "Create");
        //load data
        Location root = LocationTree.get(getApplicationContext()).getRoot();
        LessonLab.get(getApplicationContext());
        mapView = new MapView(getApplicationContext(), R.drawable.conjunto);

        //set notification alarms
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean isOn = prefs.getBoolean(LessonAlarms.PREF_IS_ALARM_ON, false);
        LessonAlarms.setAlarms(getApplicationContext(), isOn);

        //initialize
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
        navigationView.setCheckedItem(R.id.nav_home);
        processExtraData();
    }

    private void processExtraData(){
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            int mapId = extras.getInt(EXTRA_MAP_ID, -1);
            if(mapId != -1){
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        MapFragment.newInstance(this, mapId), MAP_FRAGMENT_TAG).commit();
                navigationView.setCheckedItem(R.id.nav_map);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        changeFragment(menuItem.getItemId());
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {
        HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(HOME_FRAGMENT_TAG);

        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        } else if(homeFragment == null || !homeFragment.isVisible() ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HomeFragment(), HOME_FRAGMENT_TAG).commit();
            navigationView.setCheckedItem(R.id.nav_home);
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getApplicationContext().getSharedPreferences(MapFragment.PREFS_FILE, Context.MODE_PRIVATE)
                    .edit().putBoolean(MapFragment.PREF_HAS_PERMISSIONS, true).commit();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
}
