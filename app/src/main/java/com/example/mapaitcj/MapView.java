package com.example.mapaitcj;

import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.util.AttributeSet;

public class MapView extends TouchImageView {
    public static final String TAG = "MapView";

    public static final String UPDATE_INTERFACE = "com.example.mapaitcj.UPDATE_INTERFACE";
    public static final String PERM_PRIVATE = "com.example.mapaitcj.PRIVATE";

    public MapView(Context context){
        this(context, null);
    }

    public MapView(Context context, int imageId){
        super(context, null);
        setRotateImageToFitScreen(false);
        setImageResource(imageId);
    }

    public MapView(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        setRotateImageToFitScreen(false);
        setImageResource(R.drawable.conjunto);
    }
    public MapView(Context context, AttributeSet attributeSet, float scale, float coordX, float coordY ){
        super(context, attributeSet);
        setRotateImageToFitScreen(false);
        setImageResource(R.drawable.conjunto);
        setZoom(scale, coordX, coordY);
    }

    @Override
    public void updateView(Matrix matrix) {
        super.updateView(matrix);
        getContext().sendBroadcast(new Intent(UPDATE_INTERFACE), PERM_PRIVATE);
    }


}
