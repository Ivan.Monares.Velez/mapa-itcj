package com.example.mapaitcj;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class HomeFragment extends Fragment {

    private Button mMapButton;
    private Button mSearchButton;
    private Button mScheduleButton;
    private Button mFaqButton;
    private ImageButton mSettingsButton;

    private FragmentChanger fragmentChanger;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mMapButton = view.findViewById(R.id.map_button);
        mMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentChanger != null) {
                    fragmentChanger.changeFragment(R.id.nav_map);
                }
            }
        });

        mSearchButton = view.findViewById(R.id.search_map_button);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentChanger != null) {
                    fragmentChanger.changeFragment(R.id.nav_search);
                }
            }
        });

        mScheduleButton = view.findViewById(R.id.schedule_button);
        mScheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentChanger != null) {
                    fragmentChanger.changeFragment(R.id.nav_schedule);
                }
            }
        });

        mFaqButton = view.findViewById(R.id.faq_button);
        mFaqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentChanger != null) {
                    fragmentChanger.changeFragment(R.id.nav_faq);
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentChanger = (FragmentChanger) context;
        } catch (ClassCastException castException) {
            fragmentChanger = null;
        }
    }

    public interface FragmentChanger{
        void changeFragment(int id);
    }
}
