package com.example.mapaitcj;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SelectFragment extends DialogFragment {

    public static final String EXTRA_LOCATION_ID = "com.example.mapaitcj.extra_location_id";

    private ExpandableListView mExpandableListView;
    private EditText mSearchField;
    private LinearLayout mNoMatchMessage;

    private ELVAdapter mELVAdapter;
    private ArrayList<String> mListCategories;
    private Map<String, ArrayList<String>> mMapChild;
    private Map<String, ArrayList<Integer>> mMapChildIds;

    private ArrayList<String> mCurrentMapCategories;
    private Map<String, ArrayList<Integer>> mCurrentMapChildIds;

    private int chosen;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_search, null);

        mExpandableListView = view.findViewById(R.id.expLV);
        mSearchField = view.findViewById(R.id.et_search);
        mNoMatchMessage = view.findViewById(R.id.no_match);
        mNoMatchMessage.setVisibility(View.GONE);

        mListCategories = new ArrayList<>();
        mMapChild = new HashMap<>();
        mMapChildIds = new HashMap<>();

        loadList();

        mELVAdapter = new ELVAdapter(mListCategories, mMapChild, getContext());
        mExpandableListView.setAdapter(mELVAdapter);
        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int position, int childPosition, long l) {

                chosen = mCurrentMapChildIds.get(mCurrentMapCategories.get(position)).get(childPosition);
                sendResult(Activity.RESULT_OK);
                dismiss();
                return true;
            }
        });

        mSearchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mSearchField.getText().toString().length() == 0) {
                    mELVAdapter = new ELVAdapter(mListCategories, mMapChild, getContext());
                    mExpandableListView.setAdapter(mELVAdapter);


                    mCurrentMapCategories = mListCategories;
                    mCurrentMapChildIds = mMapChildIds;
                } else {

                    HashMap<String, ArrayList<Integer>> hashMap_searchIds = new HashMap<>();
                    HashMap<String, ArrayList<String>> hashMap_search = new HashMap<>();
                    ArrayList<String>  filtered_groups = new ArrayList<>();

                    for (int i = 0; i < mListCategories.size(); i++) {
                        ArrayList<String> filtered_chidren = new ArrayList<>();
                        ArrayList<Integer> filtered_chidren_ids = new ArrayList<>();

                        ArrayList<String> children = mMapChild.get(mListCategories.get(i));
                        ArrayList<Integer> childrenIds = mMapChildIds.get(mListCategories.get(i));
                        for (int j = 0; j < children.size(); j++) {
                            if (children.get(j).toLowerCase().contains(mSearchField.getText().toString())) {
                                filtered_chidren.add(children.get(j));
                                filtered_chidren_ids.add(childrenIds.get(j));
                            }
                        }

                        if (filtered_chidren.size() != 0) {
                            filtered_groups.add(mListCategories.get(i));
                            hashMap_search.put(mListCategories.get(i), filtered_chidren);
                            hashMap_searchIds.put(mListCategories.get(i), filtered_chidren_ids);
                        }
                    }


                    mELVAdapter = new ELVAdapter(filtered_groups, hashMap_search, getContext());
                    mExpandableListView.setAdapter(mELVAdapter);

                    mCurrentMapCategories = filtered_groups;
                    mCurrentMapChildIds = mMapChildIds;

                    for (int i = 0; i < mELVAdapter.getGroupCount(); i++) {
                        mExpandableListView.expandGroup(i);
                    }
                }

                if(mExpandableListView.getCount() == 0){
                    mNoMatchMessage.setVisibility(View.VISIBLE);
                } else {
                    mNoMatchMessage.setVisibility(View.GONE);
                }
            }
        });


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view).setTitle(R.string.class_location);
        return builder.create();
    }

    private void loadList(){

        String[] listOptions;
        ArrayList<String> itemOptions;
        ArrayList<String> itemList;
        int count = 0;

        LocationTree lt = LocationTree.get(getContext());
        Location root = lt.getRoot();
        addToMap(root);

        for (int locationId: root.getChildrenIds() ) {
            addToMap(lt.findById(locationId));
        }

        mCurrentMapCategories = mListCategories;
        mCurrentMapChildIds = mMapChildIds;
    }

    private void addToMap(Location location){
        if(location.getChildCount() > 0){
            mListCategories.add(location.getTitle());
            mMapChild.put(location.getTitle(), location.getChildrenNames());
            mMapChildIds.put(location.getTitle(), location.getChildrenIds());
        }
    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() == null)
            return;
        Intent i = new Intent();
        i.putExtra(EXTRA_LOCATION_ID, chosen);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, i);
    }

}
