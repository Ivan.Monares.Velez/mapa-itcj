package com.example.mapaitcj;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

public class ELVAdapter extends BaseExpandableListAdapter {

    private ArrayList<String> mListCategories;
    private Map<String, ArrayList<String>> mMapChild;
    private Context mContext;

    public ELVAdapter(ArrayList<String> listCategories, Map<String, ArrayList<String>> mapChild, Context context) {
        this.mListCategories = listCategories;
        this.mMapChild = mapChild;
        this.mContext = context;
    }

    @Override
    public int getGroupCount() {
        return mListCategories.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mMapChild.get(mListCategories.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mListCategories.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mMapChild.get(mListCategories.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean b, View view, ViewGroup viewGroup) {
        if(view == null){
            view = LayoutInflater.from(mContext).inflate(R.layout.elv_group, null);
        }
        String tituloCategoria = (String) getGroup(groupPosition);
        TextView textView = (TextView)view.findViewById(R.id.groupTextView);
        textView.setText(tituloCategoria);
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View view, ViewGroup viewGroup) {
        if(view == null){
            view = LayoutInflater.from(mContext).inflate(R.layout.elv_child, null);
        }
        String item = (String) getChild(groupPosition, childPosition);
        TextView textView = (TextView)view.findViewById(R.id.childTextView);
        textView.setText(item);
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
