package com.example.mapaitcj;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class LocationTree {

    private static final String TAG = "LocationTree";
    private static final String FILENAME = "locations.json";

    private HashMap<Integer, Location> mMapLocations = new HashMap<>();
    private static LocationTree sTree;
    private static int count;
    private Location mRoot;

    private JSONLocationSerializer mSerializer;

    private LocationTree(Context c) {

        mSerializer = new JSONLocationSerializer(c, FILENAME);

        initialize(c);
        try{
            mSerializer.saveLocations(mMapLocations);
            Log.d(TAG, "Saved locations");
        }catch (Exception otherE){
            Log.e(TAG, "Error saving locations", otherE);
        }

        try{
            mMapLocations = mSerializer.loadLocations();
            findRoot(mMapLocations.values().iterator().next());
            Log.d(TAG, "Loaded locations successfully");
        } catch (Exception e){
            Log.e(TAG, "Error loading locations", e);

            initialize(c);
            try{
                mSerializer.saveLocations(mMapLocations);
                Log.d(TAG, "Saved locations");
            }catch (Exception otherE){
                Log.e(TAG, "Error saving locations", otherE);
            }
        }

    }

    public static LocationTree get(Context c) {
        if (sTree == null) {
            Log.i(TAG, "creating tree");
            sTree = new LocationTree(c.getApplicationContext());
        }
        return  sTree;
    }

    public Location getRoot() {
        return mRoot;
    }

    private void findRoot(Location position){
        int parentId = position.getParentId();
        while(!(position.getId() == (parentId))){
            position = findById(parentId);
            parentId = position.getParentId();
        }
        mRoot = position;
    }

    public Location findById(int id){
        return mMapLocations.get(id);
    }

    private void addChildren(Context c, Location parent, int names, int zoom, int x, int y, int z){
        Location child = null;

        String children[]   = c.getResources().getStringArray(names);
        String xPos[]       = c.getResources().getStringArray(x);
        String yPos[]       = c.getResources().getStringArray(y);

        for(int i = 0; i < children.length; i++){
            child = new Location(count++, children[i], zoom, Double.parseDouble(xPos[i]), Double.parseDouble(yPos[i]), z);
            parent.addChild(child);
            mMapLocations.put(child.getId(), child);
        }
    }

    private void initialize(Context c){

        count  = 0;

        Log.i(TAG, "creating root");
        mRoot = new Location(count++, "Campus I", 1,0,0,0);
        mRoot.setParentId(mRoot.getId());
        addChildren(c, mRoot, R.array.Campus_I, 3, R.array.Campus_I_X, R.array.Campus_I_Y, 0);
        mMapLocations.put(mRoot.getId(), mRoot);

        ArrayList<Integer> children = mRoot.getChildrenIds();

        addChildren(c, findById(children.get(5)), R.array.Admnistracion_PB, 15, R.array.Admnistracion_PB_X, R.array.Admnistracion_PB_Y, 1);
        addChildren(c, findById(children.get(5)), R.array.Admnistracion_PA, 15, R.array.Admnistracion_PA_X, R.array.Admnistracion_PA_Y, 2);

        addChildren(c, findById(children.get(6)), R.array.Aulas_100s, 15, R.array.Aulas_100s_X1, R.array.Aulas_100s_Y1, 1);
        addChildren(c, findById(children.get(6)), R.array.Aulas_200s, 15, R.array.Aulas_200s_X2, R.array.Aulas_200s_Y2, 2);

        addChildren(c, findById(children.get(7)), R.array.Ciencias_basicas_PB, 15, R.array.Ciencias_basicas_X1, R.array.Ciencias_basicas_Y1, 1);
        addChildren(c, findById(children.get(7)), R.array.Ciencias_basicas_PA, 15, R.array.Ciencias_basicas_X2, R.array.Ciencias_basicas_Y2, 2);

        addChildren(c, findById(children.get(8)), R.array.Aulas_400s_PB, 15, R.array.Aulas_400s_X1, R.array.Aulas_400s_Y1, 1);
        addChildren(c, findById(children.get(8)), R.array.Aulas_400s_PA, 15, R.array.Aulas_400s_X2, R.array.Aulas_400s_Y2, 2);

        addChildren(c, findById(children.get(9)), R.array.Lab_Electrónica, 15, R.array.Lab_Electrónica_X, R.array.Lab_Electrónica_Y, 1);

        addChildren(c, findById(children.get(10)), R.array.Quimica, 15, R.array.Quimica_X, R.array.Quimica_Y, 1);

        addChildren(c, findById(children.get(11)), R.array.Metal_Mecanica_PB, 15, R.array.Metal_Mecanica_PB_X, R.array.Metal_Mecanica_PB_Y, 1);
        addChildren(c, findById(children.get(11)), R.array.Metal_Mecanica_PA, 15, R.array.Metal_Mecanica_PA_X, R.array.Metal_Mecanica_PA_Y, 2);

        addChildren(c, findById(children.get(12)), R.array.Laboratorio_Eléctrica_PB, 15, R.array.Laboratorio_Eléctrica_X1, R.array.Laboratorio_Eléctrica_Y1, 1);
        addChildren(c, findById(children.get(12)), R.array.Laboratorio_Eléctrica_PA, 15, R.array.Laboratorio_Eléctrica_X2, R.array.Laboratorio_Eléctrica_Y2, 2);

        addChildren(c, findById(children.get(13)), R.array.Manufactura, 15, R.array.Manufactura_X, R.array.Manufactura_Y, 1);

        addChildren(c, findById(children.get(14)), R.array.SistemasPB, 15, R.array.Sistemas_X1, R.array.Sistemas_Y1, 1);
        addChildren(c, findById(children.get(14)), R.array.SistemasPA, 15, R.array.Sistemas_X2, R.array.Sistemas_Y2, 2);

        addChildren(c, findById(children.get(15)), R.array.Aulas_800s_PB, 15, R.array.Aulas_800s_X1, R.array.Aulas_800s_Y1, 1);
        addChildren(c, findById(children.get(15)), R.array.Aulas_800s_PA, 15, R.array.Aulas_800s_X2, R.array.Aulas_800s_Y2, 2);

        addChildren(c, findById(children.get(16)), R.array.Industrial_PB, 15, R.array.Industrial_PB_X, R.array.Industrial_PB_Y, 1);
        addChildren(c, findById(children.get(16)), R.array.Industrial_PA, 15, R.array.Industrial_PA_X, R.array.Industrial_PA_Y, 2);

        addChildren(c, findById(children.get(17)), R.array.Guillot, 15, R.array.Guillot_X, R.array.Guillot_Y, 1);

        addChildren(c, findById(children.get(18)), R.array.Posgrado_PB, 15, R.array.Posgrado_PB_X, R.array.Posgrado_PB_Y, 1);
        addChildren(c, findById(children.get(18)), R.array.Posgrado_PA, 15, R.array.Posgrado_PA_X, R.array.Posgrado_PA_Y, 2);

        addChildren(c, findById(children.get(19)), R.array.Mecatronica, 15, R.array.Mecatronica_X, R.array.Mecatronica_Y, 1);

		//audiovisual 2

        addChildren(c, findById(children.get(21)), R.array.Centro_de_computo, 15, R.array.Centro_de_computo_X, R.array.Centro_de_computo_Y, 1);

        addChildren(c, findById(children.get(22)), R.array.División_de_estudios, 15, R.array.División_de_estudios_X, R.array.División_de_estudios_Y, 1);

        addChildren(c, findById(children.get(23)), R.array.Gestion, 15, R.array.Gestion_X, R.array.Gestion_Y, 1);

        addChildren(c, findById(children.get(24)), R.array.Centro_de_idiomas, 15, R.array.Centro_de_idiomas_X, R.array.Centro_de_idiomas_Y, 1);

        addChildren(c, findById(children.get(25)), R.array.Biblioteca_PB, 15, R.array.Biblioteca_PB_X, R.array.Biblioteca_PB_Y, 1);
        addChildren(c, findById(children.get(25)), R.array.Biblioteca_PA, 15, R.array.Biblioteca_PA_X, R.array.Biblioteca_PA_Y, 2);

        addChildren(c, findById(children.get(26)), R.array.Gimnasio_PB, 15, R.array.Gimnasio_PB_X, R.array.Gimnasio_PB_Y, 1);
        addChildren(c, findById(children.get(26)), R.array.Gimnasio_PA, 15, R.array.Gimnasio_PA_X, R.array.Gimnasio_PA_Y, 2);

        addChildren(c, findById(children.get(27)), R.array.Alberca_PB, 15, R.array.Alberca_X1, R.array.Alberca_Y1, 1);
        addChildren(c, findById(children.get(27)), R.array.Alberca_PA, 15, R.array.Alberca_X2, R.array.Alberca_Y2, 2);

        addChildren(c, findById(children.get(28)), R.array.Almacen, 15, R.array.Almacen_X, R.array.Almacen_Y, 1);

        addChildren(c, findById(children.get(29)), R.array.Mantenimiento, 15, R.array.Mantenimiento_X, R.array.Mantenimiento_Y, 1);
    }

}
