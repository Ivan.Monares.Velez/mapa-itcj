package com.example.mapaitcj;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class JSONLocationSerializer {

    private static final String TAG = "JSONSerializer";

    private Context mContext;
    private String mFilename;

    public JSONLocationSerializer(Context c, String f){
        mContext = c;
        mFilename = f;
    }

    public void saveLocations(HashMap<Integer, Location> locationMap) throws JSONException, IOException {
        //Build an array in JSON
        JSONArray array = new JSONArray();

        for(Location l: locationMap.values()){
            array.put(l.toJSON());
        }

        Writer writer = null;
        try {
            OutputStream outputStream = mContext.openFileOutput(mFilename, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(outputStream);
            writer.write(array.toString());
            Log.d(TAG, "added location");
        } finally {
            if(writer != null){
                writer.close();
            }
        }
    }

    public HashMap<Integer, Location> loadLocations() throws JSONException, IOException {
        HashMap<Integer, Location> locations = new HashMap<>();
        BufferedReader reader = null;
        try {
            //Open and read file into a String Builder
            InputStream inputStream = mContext.openFileInput(mFilename);
            reader = new BufferedReader( new InputStreamReader(inputStream));

            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null){
                //Line breaks are omitted and irrelevant
                jsonString.append(line);
            }

            //Parse the JSON and build the array of lessons from JSONObjects
            JSONArray array = new JSONArray(jsonString.toString());

            for(int i = 0; i < array.length(); i++){
                Location location = new Location(array.getJSONObject(i));
                locations.put(location.getId(), location);
            }
        } catch (FileNotFoundException e){
            Log.d(TAG, "FNF");
            //Ignore this one, only happens when starting fresh
        } finally {
            if(reader != null){
                reader.close();
            }
        }
        return locations;
    }
}
