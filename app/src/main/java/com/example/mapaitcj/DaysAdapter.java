package com.example.mapaitcj;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.ViewHolder> {

    private String[] mDataset;
    private boolean[] active;
    private Context context;
    private int layoutResource;

    // Provide a suitable constructor (depends on the kind of dataset)
    public DaysAdapter(Context context, String[] myDataset, int resource) {
        this.mDataset = myDataset;
        this.active = new boolean[7];
        this.context = context;
        this.layoutResource = resource;
    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public DaysAdapter(Context context, String[] myDataset, boolean[] active, int resource) {
        this.mDataset = myDataset;
        this.active = active;
        this.context = context;
        this.layoutResource = resource;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        TextView v = (TextView) LayoutInflater.from(parent.getContext()).inflate(layoutResource, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mDataset[position], active[position], context);
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;

        public ViewHolder(TextView v) {
            super(v);
            textView = v;
        }

        public void bind(final String itemTitle, final boolean isActive, Context c) {

            textView.setText(itemTitle);
            textView.setTypeface(null, Typeface.BOLD);

            if(isActive){
                textView.setTextColor(c.getResources().getColor(R.color.colorSecondary));
            } else {
                textView.setTextColor(c.getResources().getColor(R.color.darkGrey));
            }
        }

    }
}