package com.example.mapaitcj;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class DaysEditorAdapter extends RecyclerView.Adapter<DaysEditorAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(View v, int position);
    }

    private final OnItemClickListener listener;
    private String[] mDataset;
    private boolean[] active;
    private Context context;
    private int layoutResource;

    // Provide a suitable constructor (depends on the kind of dataset)
    public DaysEditorAdapter(Context context, String[] myDataset, int resource, OnItemClickListener listener) {
        this.mDataset = myDataset;
        this.listener = listener;
        this.active = new boolean[7];
        this.context = context;
        this.layoutResource = resource;
    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public DaysEditorAdapter(Context context, String[] myDataset, boolean[] active, int resource, OnItemClickListener listener) {
        this.mDataset = myDataset;
        this.listener = listener;
        this.active = active;
        this.context = context;
        this.layoutResource = resource;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        TextView v = (TextView) LayoutInflater.from(parent.getContext()).inflate(layoutResource, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        holder.bind(position, mDataset[position], active[position], listener, context);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;

        public ViewHolder(TextView v) {
            super(v);
            textView = v;
        }

        public void bind(final int position, final String itemTitle, final boolean isActive, final OnItemClickListener listener, Context c) {

            textView.setText(itemTitle);
            textView.setTypeface(null, Typeface.BOLD);

            if(isActive){
                textView.setBackgroundColor(c.getResources().getColor(R.color.colorSecondary));
                textView.setTextColor(c.getResources().getColor(R.color.colorOnSecondary));
            } else {
                textView.setBackgroundColor(c.getResources().getColor(R.color.colorPrimary));
                textView.setTextColor(c.getResources().getColor(R.color.colorOnPrimary));
            }

            textView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(v, position);
                }
            });
        }
    }
}