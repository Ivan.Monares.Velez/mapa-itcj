package com.example.mapaitcj;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.navigation.NavigationView;

public class FaqFragment extends Fragment {

    private LinearLayout mFAQContainer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (ScrollView) inflater.inflate(R.layout.fragment_faq, container, false);

        mFAQContainer = (LinearLayout) view.findViewById(R.id.faq_container);

        String titles[]   = getContext().getResources().getStringArray(R.array.faq_titles);
        String contents[]   = getContext().getResources().getStringArray(R.array.faq_contenta);
        String schedules[]   = getContext().getResources().getStringArray(R.array.faq_schedules);
        String locations[]   = getContext().getResources().getStringArray(R.array.faq_locations);
        String locationIdString[]   = getContext().getResources().getStringArray(R.array.faq_location_ids);
        int locationIds[] = new int[locationIdString.length];
        for(int i = 0; i < locationIdString.length; i++){
            locationIds[i] = Integer.parseInt(locationIdString[i]);
        }

        for(int i =  0; i < titles.length; i++) {
            addQuestionCard(-1, titles[i], contents[i], schedules[i], locations[i], locationIds[i]);
        }

        return view;
    }

    public void addQuestionCard(int imageResource, String title, String content, String schedule, String locationInfo, final int locationId ){
        MaterialCardView card = (MaterialCardView) getActivity().getLayoutInflater().inflate(R.layout.faq_card, null);
        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(dpToPx(8), dpToPx(8), dpToPx(8), dpToPx(8));
        card.setLayoutParams(layoutParams);

        ImageView image = (ImageView) card.findViewById(R.id.faq_card_image);
        if(imageResource != -1) {
            image.setImageResource(R.drawable.ic_launcher_background);
        } else {
            image.setVisibility(View.GONE);
        }

        TextView titleView = (TextView) card.findViewById(R.id.faq_card_title);
        titleView.setText(title);

        TextView contentView = (TextView) card.findViewById(R.id.faq_card_content);
        contentView.setText(content);

        TextView scheduleView = (TextView) card.findViewById(R.id.faq_card_schedule);
        scheduleView.setText("Horario: " + schedule);

        TextView locationInfoView = (TextView) card.findViewById(R.id.faq_card_location);
        locationInfoView.setText("Para más información acudir a " + locationInfo);

        AppCompatButton buttonToMap = (AppCompatButton) card.findViewById(R.id.faq_map_button);
        buttonToMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MapFragment mapFragment = MapFragment.newInstance(getActivity(), locationId);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        mapFragment, MainActivity.MAP_FRAGMENT_TAG).commit();
                ((NavigationView)getActivity().findViewById(R.id.nav_view)).setCheckedItem(R.id.nav_map);
            }
        });

        mFAQContainer.addView(card);
    }

    public int dpToPx(double dpValue) {
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return  (int) ( dpValue * ((double)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT) );
    }
}
