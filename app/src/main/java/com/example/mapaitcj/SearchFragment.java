package com.example.mapaitcj;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchFragment extends Fragment {

    private ExpandableListView mExpandableListView;
    private EditText mSearchField;
    private LinearLayout mNoMatchMessage;

    private ELVAdapter mELVAdapter;
    private ArrayList<String> mListCategories;
    private Map<String, ArrayList<String>> mMapChild;
    private Map<String, ArrayList<Integer>> mMapChildIds;

    private ArrayList<String> mCurrentMapCategories;
    private Map<String, ArrayList<Integer>> mCurrentMapChildIds;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        mExpandableListView = view.findViewById(R.id.expLV);
        mSearchField = view.findViewById(R.id.et_search);
        mNoMatchMessage = view.findViewById(R.id.no_match);
        mNoMatchMessage.setVisibility(View.GONE);

        mListCategories = new ArrayList<>();
        mMapChild = new HashMap<>();
        mMapChildIds = new HashMap<>();

        loadList();

        mELVAdapter = new ELVAdapter(mListCategories, mMapChild, getContext());
        mExpandableListView.setAdapter(mELVAdapter);
        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int position, int childPosition, long l) {

                int id = mCurrentMapChildIds.get(mCurrentMapCategories.get(position)).get(childPosition);

                hideKeyboard(getActivity());

                MapFragment mapFragment = MapFragment.newInstance(getActivity(), id);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        mapFragment, MainActivity.MAP_FRAGMENT_TAG).commit();
                ((NavigationView)getActivity().findViewById(R.id.nav_view)).setCheckedItem(R.id.nav_map);
                return true;
            }
        });

        mSearchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mSearchField.getText().toString().length() == 0) {
                    mELVAdapter = new ELVAdapter(mListCategories, mMapChild, getContext());
                    mExpandableListView.setAdapter(mELVAdapter);

                    mCurrentMapCategories = mListCategories;
                    mCurrentMapChildIds = mMapChildIds;
                } else {

                    HashMap<String, ArrayList<Integer>> hashMap_searchIds = new HashMap<>();
                    HashMap<String, ArrayList<String>> hashMap_search = new HashMap<>();
                    ArrayList<String>  filtered_groups = new ArrayList<>();

                    for (int i = 0; i < mListCategories.size(); i++) {
                        ArrayList<String> filtered_chidren = new ArrayList<>();
                        ArrayList<Integer> filtered_chidren_ids = new ArrayList<>();

                        ArrayList<String> children = mMapChild.get(mListCategories.get(i));
                        ArrayList<Integer> childrenIds = mMapChildIds.get(mListCategories.get(i));
                        for (int j = 0; j < children.size(); j++) {
                            if (children.get(j).toLowerCase().contains(mSearchField.getText().toString())) {
                                filtered_chidren.add(children.get(j));
                                filtered_chidren_ids.add(childrenIds.get(j));
                            }
                        }

                        if (filtered_chidren.size() != 0) {
                            filtered_groups.add(mListCategories.get(i));
                            hashMap_search.put(mListCategories.get(i), filtered_chidren);
                            hashMap_searchIds.put(mListCategories.get(i), filtered_chidren_ids);
                        }
                    }


                    mELVAdapter = new ELVAdapter(filtered_groups, hashMap_search, getContext());
                    mExpandableListView.setAdapter(mELVAdapter);

                    mCurrentMapCategories = filtered_groups;
                    mCurrentMapChildIds = hashMap_searchIds;

                    for (int i = 0; i < mELVAdapter.getGroupCount(); i++) {
                        mExpandableListView.expandGroup(i);
                    }
                }

                if(mExpandableListView.getCount() == 0){
                    mNoMatchMessage.setVisibility(View.VISIBLE);
                } else {
                    mNoMatchMessage.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void loadList(){

        String[] listOptions;
        ArrayList<String> itemOptions;
        ArrayList<String> itemList;
        int count = 0;

        LocationTree lt = LocationTree.get(getContext());
        Location root = lt.getRoot();
        addToMap(root);

        for (int locationId: root.getChildrenIds() ) {
            addToMap(lt.findById(locationId));
        }

        mCurrentMapCategories = mListCategories;
        mCurrentMapChildIds = mMapChildIds;
    }

    private void addToMap(Location location){
        if(location.getChildCount() > 0){
            mListCategories.add(location.getTitle());
            mMapChild.put(location.getTitle(), location.getChildrenNames());
            mMapChildIds.put(location.getTitle(), location.getChildrenIds());
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
