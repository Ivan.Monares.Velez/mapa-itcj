package com.example.mapaitcj;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

public class JSONLessonSerializer {

    private static final String TAG = "JSONSerializer";

    private Context mContext;
    private String mFilename;

    public JSONLessonSerializer(Context c, String f){
        mContext = c;
        mFilename = f;
    }

    public void saveLessons(ArrayList<Lesson> lessons) throws JSONException, IOException {
        //Build an array in JSON
        JSONArray array = new JSONArray();
        for(Lesson c: lessons){
            array.put(c.toJSON());
        }

        Writer writer = null;
        try {
            OutputStream outputStream = mContext.openFileOutput(mFilename, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(outputStream);
            writer.write(array.toString());
        } finally {
            if(writer != null){
                writer.close();
            }
        }
    }

    public ArrayList<Lesson> loadLessons() throws JSONException, IOException {
        ArrayList<Lesson> lessons = new ArrayList<Lesson>();
        BufferedReader reader = null;
        try {
            //Open and read file into a String Builder
            InputStream inputStream = mContext.openFileInput(mFilename);
            reader = new BufferedReader( new InputStreamReader(inputStream));

            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null){
                //Line breaks are omitted and irrelevant
                jsonString.append(line);
            }

            //Parse the JSON and build the array of lessons from JSONObjects
            JSONArray array = new JSONArray(jsonString.toString());

            for(int i = 0; i < array.length(); i++){
                lessons.add(new Lesson(array.getJSONObject(i)));
            }
        } catch (FileNotFoundException e){
            Log.d(TAG, "FNF");
            //Ignore this one, only happens when starting fresh
        } finally {
            if(reader != null){
                reader.close();
            }
        }
        return lessons;
    }
}
