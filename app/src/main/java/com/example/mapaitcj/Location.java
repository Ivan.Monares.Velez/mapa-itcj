package com.example.mapaitcj;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Location {

    private static final String TAG = "LocationClass";

    private static final String JSON_ID = "id";
    private static final String JSON_TITLE = "title";
    private static final String JSON_ZOOM = "zoom";
    private static final String JSON_COORDX = "coordx";
    private static final String JSON_COORDY = "coordy";
    private static final String JSON_LEVELZ = "levelz";
    private static final String JSON_PARENT_ID = "parent_id";
    private static final String JSON_CHILD_COUNT = "children_amount";

    private int mId;
    private String mTitle;
    private int mZoom;
    private double mCoordX;
    private double mCoordY;
    private int mLevelZ;
    private int mParentId;
    private ArrayList<String> mChildrenNames = new ArrayList<>();
    private ArrayList<Integer> mChildrenIds = new ArrayList<>();

    public Location(int id, String title, int zoom, double x, double y, int z){

        mId = id;
        mTitle = title;
        mZoom = zoom;
        mCoordX = x;
        mCoordY = y;
        mLevelZ = z;
    }

    public Location(JSONObject json) throws JSONException {
        mId = json.getInt(JSON_ID);
        mTitle = json.getString(JSON_TITLE);
        mZoom = json.getInt(JSON_ZOOM);
        mCoordX = json.getDouble(JSON_COORDX);
        mCoordY = json.getDouble(JSON_COORDY);
        mLevelZ = json.getInt(JSON_LEVELZ);
        mParentId = json.getInt(JSON_PARENT_ID);

        int count = json.getInt(JSON_CHILD_COUNT);

        mChildrenNames = new ArrayList<>();
        mChildrenIds = new ArrayList<>();

        for(int i = 0; i < count; i++){
            String name = json.getString("name"+i);
            int id = json.getInt("id"+i);

            mChildrenIds.add(id);
            mChildrenNames.add(name);
        }
    }

    public int getId() {
        return mId;
    }

    public String getTitle(){
        return mTitle;
    }

    public int getZoom() {
        return mZoom;
    }

    public double getCoordX() {
        return mCoordX;
    }

    public double getCoordY() {
        return mCoordY;
    }

    public int getLevelZ() {
        return mLevelZ;
    }

    public int getParentId() {
        return mParentId;
    }

    public ArrayList<String> getChildrenNames() {
        return mChildrenNames;
    }

    public ArrayList<Integer> getChildrenIds() {
        return mChildrenIds;
    }

    public int getChildCount() {
        return mChildrenIds.size();
    }

    public void setParentId(int parentId){
        mParentId = parentId;
    }

    public void setParent(Location mParent) {
        this.mParentId = mParent.getId();
    }

    public void addChild(Location child){
        if(child == null) return;

        mChildrenNames.add(child.getTitle());
        mChildrenIds.add(child.getId());
        child.setParent(this);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, mId);
        json.put(JSON_TITLE, mTitle);
        json.put(JSON_ZOOM, mZoom);
        json.put(JSON_COORDX, mCoordX);
        json.put(JSON_COORDY, mCoordY);
        json.put(JSON_LEVELZ, mLevelZ);
        json.put(JSON_PARENT_ID, mParentId);
        json.put(JSON_CHILD_COUNT, getChildCount());

        for(int i = 0; i < getChildCount(); i++){
            json.put("name"+i, mChildrenNames.get(i));
            json.put("id"+i, mChildrenIds.get(i));
        }

        return json;
    }
}
