package com.example.mapaitcj;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.UUID;

public class ScheduleEditorFragment extends DialogFragment {

    public static final String TAG = "ScheduleEditorFragment";

    public static final String DIALOG_LOCATION = "com.example.mapaitcj.dialog_location";
    public static final int REQUEST_LOCATION = 0;

    public static final String EXTRA_ID = "com.example.mapaitcj.extra_id";
    public static final String EXTRA_LOCATION_ID = "com.example.mapaitcj.extra_location_id";
    public static final String EXTRA_TITLE = "com.example.mapaitcj.extra_title";
    public static final String EXTRA_HOUR = "com.example.mapaitcj.extra_hour";
    public static final String EXTRA_MINUTE = "com.example.mapaitcj.extra_minute";
    public static final String EXTRA_DAYS = "com.example.mapaitcj.extra_days";

    private Button mChooseLocation;
    private EditText mTitleView;
    private TimePicker mTimePicker;
    private RecyclerView mDaysList;
    private Button mPositiveButton;
    private Button mCancelButton;

    private String[] mDayOptions = {"D", "L", "M", "M", "J", "V", "S"};

    private UUID mLessonId;
    private Location mLocation;
    private int mHour;
    private int mMinute;
    private boolean[] mDayBoolean;


    public static ScheduleEditorFragment newInstance(UUID id, int locationId, String title, int hour, int minute, int days) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_ID, id);
        args.putInt(EXTRA_LOCATION_ID, locationId);
        args.putString(EXTRA_TITLE, title);
        args.putInt(EXTRA_HOUR, hour);
        args.putInt(EXTRA_MINUTE, minute);
        args.putInt(EXTRA_DAYS, days);
        ScheduleEditorFragment fragment = new ScheduleEditorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        View view = getActivity().getLayoutInflater().inflate(R.layout.schedule_editor, null);

        mLessonId = (UUID) getArguments().getSerializable(EXTRA_ID);
        String title = getArguments().getString(EXTRA_TITLE, "");
        int locId = getArguments().getInt(EXTRA_LOCATION_ID, -1);
        if (locId != -1) {
            mLocation = LocationTree.get(getContext()).findById(locId);
        } else {
            mLocation = LocationTree.get(getContext()).getRoot();
        }
        mHour = getArguments().getInt(EXTRA_HOUR, 7);
        ;
        mMinute = getArguments().getInt(EXTRA_MINUTE, 0);
        int days = getArguments().getInt(EXTRA_DAYS, 0);
        mDayBoolean = Lesson.getClassDaysFromInt(days);

        mTitleView = view.findViewById(R.id.schedule_title);
        mTitleView.setText(title);
        mTitleView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Bundle args = getArguments();
                args.putString(EXTRA_TITLE, mTitleView.getText().toString());
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mDaysList = view.findViewById(R.id.days_checkable_list);
        mDaysList.setLayoutManager(layoutManager);
        mDaysList.setHasFixedSize(true);
        final DaysEditorAdapter daysAdapter = new DaysEditorAdapter(getContext(), mDayOptions, mDayBoolean, R.layout.list_item_days_editable, new DaysEditorAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mDayBoolean[position] = !mDayBoolean[position];
                if (mDayBoolean[position]) {
                    view.setBackgroundColor(getResources().getColor(R.color.colorSecondary));
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorOnSecondary));
                } else {
                    view.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorOnPrimary));
                }
                getArguments().putInt(EXTRA_DAYS, Lesson.getClassDaysAsInt(mDayBoolean));
            }
        });
        mDaysList.setAdapter(daysAdapter);

        mTimePicker = view.findViewById(R.id.timePicker);
        mTimePicker.setCurrentHour(mHour);
        mTimePicker.setCurrentMinute(mMinute);
        mTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hourOfDay, int minute) {
                mHour = hourOfDay;
                mMinute = minute;
                getArguments().putInt(EXTRA_HOUR, hourOfDay);
                getArguments().putInt(EXTRA_MINUTE, minute);
            }
        });

        mChooseLocation = view.findViewById(R.id.choose_location);
        mChooseLocation.setText(mLocation.getTitle());
        mChooseLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                SelectFragment dialog = new SelectFragment();
                dialog.setTargetFragment(ScheduleEditorFragment.this, REQUEST_LOCATION);
                dialog.show(fm, DIALOG_LOCATION);
            }
        });

        mPositiveButton = view.findViewById(R.id.positiveButton);
        mPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Lesson.getClassDaysAsInt(mDayBoolean) > 0) {
                    saveLesson();
                    dismiss();
                } else {
                    new AlertDialog.Builder(getContext())
                            .setTitle("Días de clase flatantes")
                            .setMessage("Por favor selecciona por lo menos un día de clase.")
                            .setPositiveButton(android.R.string.yes, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }
        });

        mCancelButton = view.findViewById(R.id.cancelButton);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view).setTitle(R.string.edit_schedule);
        /*
        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                saveLesson();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Close dialog
            }
        });
        */
        return builder.create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_LOCATION) {
            int id = data.getIntExtra(SelectFragment.EXTRA_LOCATION_ID, 0);
            mLocation = LocationTree.get(getContext()).findById(id);
            mChooseLocation.setText(mLocation.getTitle());
            getArguments().putSerializable(EXTRA_LOCATION_ID, id);

        }
    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() == null)
            return;
        Intent i = new Intent();
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, i);
    }

    private void saveLesson() {
        if (mLessonId != null) {
            Lesson l = LessonLab.get(getContext()).getLesson(mLessonId);

            String title = mTitleView.getText().toString();

            if (title.isEmpty()) {
                title = "Nueva Clase";
            }

            l.setLocationId(mLocation.getId());
            l.setTitle(title);
            l.setHour(mHour);
            l.setMinute(mMinute);
            l.setClassDays(mDayBoolean);
            sendResult(Activity.RESULT_OK);
        } else {
            String title = mTitleView.getText().toString();

            if (title.isEmpty()) {
                title = "Nueva Clase";
            }
            Lesson lesson = new Lesson(mLocation.getId(), title, mHour, mMinute, mDayBoolean);
            LessonLab.get(getContext()).addLesson(lesson);
            sendResult(Activity.RESULT_OK);
        }
    }
}
