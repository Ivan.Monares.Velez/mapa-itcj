package com.example.mapaitcj;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

public class LessonAlarms extends BroadcastReceiver {
    private static final String TAG = "LessonAlarms";
    public static final String PREF_IS_ALARM_ON = "isAlarmOn";
    public static final String ACTION_SHOW_NOTIFICATION = "com.example.mapaitcj.SHOW_NOTIFICATION";
    public static final String PERM_PRIVATE = "com.example.mapaitcj.PRIVATE";

    private static final String PREF_LESSON_ID = "LessonId";

    @Override
    public void onReceive(Context context, @Nullable Intent intent) {
        if (!(intent.getAction().equals(TAG))) return;

        Log.i(TAG, "onHandleIntent");
        Resources r = context.getResources();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String lessonIdString = prefs.getString(LessonAlarms.PREF_LESSON_ID, "");

        if(lessonIdString.isEmpty()) return;

        UUID lessonId = UUID.fromString( lessonIdString );
        Lesson lesson = LessonLab.get(context).getLesson(lessonId);

        Location location = LocationTree.get(context).findById(lesson.getLocationId());

        Intent mapIntent = new Intent(context, MainActivity.class);
        mapIntent.putExtra(MainActivity.EXTRA_MAP_ID, lesson.getLocationId());
        String content = String.format(r.getString(R.string.notification_content), lesson.getTitle(), location.getTitle());

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "notify_001");

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, mapIntent, 0);
        pendingIntent.cancel();
        pendingIntent = PendingIntent.getActivity(context, 0, mapIntent, 0);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.setBigContentTitle(r.getString(R.string.notification_title));
        bigText.setSummaryText(content);

        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.mipmap.ic_launcher_round);
        builder.setContentTitle(r.getString(R.string.notification_title));
        builder.setContentText(content);
        builder.setStyle(bigText);
        builder.setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // === Removed some obsoletes
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
        {
            String channelId = "Your_channel_id";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
        }

        showBackgroundNotification(context, 0, builder.build());

        boolean isOn = prefs.getBoolean(LessonAlarms.PREF_IS_ALARM_ON, false);
        LessonAlarms.setAlarms(context, isOn);
    }

    void showBackgroundNotification(Context context, int requestCode, Notification notification) {

        Log.i(TAG, "before showed notification");
        Intent i = new Intent(ACTION_SHOW_NOTIFICATION);
        i.putExtra("REQUEST_CODE", requestCode);
        i.putExtra("NOTIFICATION", notification);
        context.sendOrderedBroadcast(i, PERM_PRIVATE, null, null,
                Activity.RESULT_OK, null, null);
        Log.i(TAG, "showed notification");
    }

    public static void setAlarms(Context context, boolean isOn) {

        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        Calendar repetition = Calendar.getInstance();
        LessonLab lessonLab = LessonLab.get(context);
        Lesson lesson = null;

        if(lessonLab.getCount() > 0) {
            lesson = lessonLab.getLessons().get(0);
            repetition = lesson.getNextRepetition();

            ArrayList<Lesson> lessons = LessonLab.get(context).getLessons();

            for (int i = 1; i < lessons.size(); i++) {
                Lesson l = lessons.get(i);
                Calendar r = l.getNextRepetition();

                if (r.getTimeInMillis() < repetition.getTimeInMillis()) {
                    repetition = r;
                    lesson = l;
                }
            }
        } else {
            isOn = false;
        }
        //Calculate time till next lesson
        long time = repetition.getTimeInMillis();

        Intent i = new Intent(context, LessonAlarms.class);
        i.setAction(TAG);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        PendingIntent pi = PendingIntent.getBroadcast( context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        if (isOn) {
            alarmManager.set(AlarmManager.RTC, time, pi);
            pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            SimpleDateFormat sdf = new SimpleDateFormat("EEE dd MMM yyyy HH:mm:ss", new Locale("es", "ES"));
            String s = sdf.format(repetition.getTime());
            Toast.makeText(context, "Alarma puesta para " + s, Toast.LENGTH_SHORT).show();

            PreferenceManager.getDefaultSharedPreferences(context)
                    .edit().putString(PREF_LESSON_ID, lesson.getId().toString())
                    .commit();
        } else {
            alarmManager.cancel(pi);
            pi.cancel();
            pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            Log.i(TAG, "cancel alarm");

            PreferenceManager.getDefaultSharedPreferences(context)
                    .edit().putString(PREF_LESSON_ID, "")
                    .commit();
        }

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putBoolean(LessonAlarms.PREF_IS_ALARM_ON, isOn)
                .commit();

    }

    public static boolean isAlarmActive(Context context) {
        Intent i = new Intent(context, LessonAlarms.class);
        i.setAction(TAG);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_NO_CREATE);
        return pi != null;
    }
}
