package com.example.mapaitcj;

import android.util.Log;
import android.view.MenuInflater;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.UUID;

public class Lesson {

    private static final String TAG = "Lesson";

    private static final String JSON_ID = "id";
    private static final String JSON_TITLE = "title";
    private static final String JSON_LOCATION_ID = "location_id";
    private static final String JSON_CLASS_DAYS = "class_days";
    private static final String JSON_HOUR = "hour";
    private static final String JSON_MINUTE = "minute";

    private UUID mId;
    private String mTitle;
    private int mLocationId;
    private boolean[] mClassDays;
    private int mHour;
    private int mMinute;

    public Lesson (int locationId, String title, int hour, int minute, boolean[] clasDays) {
        //Generate unique identifier
        mId = UUID.randomUUID();
        mLocationId = locationId;
        mTitle = title;
        mHour = hour;
        mMinute = minute;
        mClassDays = clasDays;
    }

    public Lesson(JSONObject json) throws JSONException {
        mId = UUID.fromString(json.getString(JSON_ID));
        mTitle = json.getString(JSON_TITLE);
        mLocationId = json.getInt(JSON_LOCATION_ID);
        mHour = json.getInt(JSON_HOUR);
        mMinute = json.getInt(JSON_MINUTE);
        setClassDays( getClassDaysFromInt(json.getInt(JSON_CLASS_DAYS)) );
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public boolean[] getClassDays() {
        return mClassDays;
    }

    public static int getClassDaysAsInt(boolean[] days){
        int value = 0;
        int mult = 1;
        for (Boolean b: days ) {
            if(b){
                value += mult;
            }
            mult *= 2;
        }
        return value;
    }

    public int getLocationId() {
        return mLocationId;
    }

    public int getHour() {
        return mHour;
    }

    public int getMinute() {
        return mMinute;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public void setClassDays(boolean[] classDays) {
        this.mClassDays = classDays;
    }

    public static boolean[] getClassDaysFromInt(int days) {
        String value = Integer.toBinaryString(days);
        value = String.format("%7s", value);
        boolean classDays[] = new boolean[7];
        for (int i = 0; i < 7; i++){
            classDays[6 - i] = (value.charAt(i) == '1');
        }

        return classDays;
    }

    public void setLocationId(int mapId) {
        this.mLocationId = mapId;
    }

    public void setHour(int hour) {
        this.mHour = hour;
    }

    public void setMinute(int minute) {
        this.mMinute = minute;
    }

    public String getFormatedTime(){
        String option = " a.m.";
        int hours = mHour;
        if(hours >= 12){
            option = " p.m.";
            hours -= 12;
        }

        if(hours == 0){
            hours = 12;
        }
        return String.format("%02d", hours) + ":" + String.format("%02d", mMinute) + option;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, mId.toString());
        json.put(JSON_TITLE, mTitle);
        json.put(JSON_LOCATION_ID, mLocationId);
        json.put(JSON_CLASS_DAYS, getClassDaysAsInt(mClassDays));
        json.put(JSON_HOUR, mHour);
        json.put(JSON_MINUTE, mMinute);

        return json;
    }

    public Calendar getNextRepetition(){
        Calendar repetition = Calendar.getInstance();
        repetition.setTimeInMillis(System.currentTimeMillis());
        repetition.set(Calendar.HOUR_OF_DAY, mHour);
        repetition.set(Calendar.MINUTE, mMinute );
        repetition.set(Calendar.SECOND, 0);

        boolean valid_day;
        int increment = 0;
        do {
            do{
                repetition.add(Calendar.HOUR_OF_DAY, increment);
                int d = repetition.get(Calendar.DAY_OF_WEEK) - 1;
                valid_day = mClassDays[d];
                increment = 24;
            }while(!valid_day);
        } while (repetition.getTimeInMillis() < System.currentTimeMillis());

        return repetition;
    }
}
