package com.example.mapaitcj;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.UUID;

public class ScheduleFragment extends ListFragment {

    private static final String TAG = "LessonsListFragment";

    private static final String DIALOG_ADD_LESSON = "addLesson";
    private static final String DIALOG_EDIT_LESSON = "editLesson";
    public static final int REQUEST_ADD_LESSON = 0;
    public static final int REQUEST_EDIT_LESSON = 1;

    private ArrayList<Lesson> mLessons;
    private Boolean mSubtitleVisible;
    private Button mNewLessonButton;
    private Button mRowButton;
    private LinearLayout mListContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        //getActivity().setTitle(R.string.nav_label_schedule);
        //getActivity().getActionBar().setSubtitle(R.string.subtitle);
        mLessons = LessonLab.get(getActivity()).getLessons();

        LessonAdapter adapter = new LessonAdapter(mLessons);
        setListAdapter(adapter);

        setRetainInstance(true);
        mSubtitleVisible = false;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_schedule, parent, false);

        mRowButton = (Button) v.findViewById(R.id.new_lesson_rowButton);
        mRowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newLesson();
            }
        });

        mNewLessonButton = (Button) v.findViewById(R.id.new_lessonButton);
        mNewLessonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newLesson();
            }
        });

        mListContainer = (LinearLayout) v.findViewById(R.id.lesson_list_container);
        updateUI();

        final ListView listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
                if (listView.getCheckedItemCount() > 1) {
                    listView.setItemChecked(i, false);
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.lesson_list_item_context, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                //
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {

                LessonAdapter adapter = (LessonAdapter) getListAdapter();
                LessonLab lessonLab = LessonLab.get(getActivity());
                Lesson l = null;
                for (int i = adapter.getCount() - 1; i >= 0; i--) {
                    if (getListView().isItemChecked(i)) {
                        l = adapter.getItem(i);
                        break;
                    }
                }
                switch (menuItem.getItemId()) {
                    case R.id.menu_item_delete_lesson:
                        lessonLab.deleteLesson(l);
                        actionMode.finish();
                        updateUI();
                        adapter.notifyDataSetChanged();

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                        boolean isOn = prefs.getBoolean(LessonAlarms.PREF_IS_ALARM_ON, false);
                        LessonAlarms.setAlarms(getContext(), isOn);

                        return true;
                    case R.id.menu_item_edit_lesson:
                        editLesson(l);
                        actionMode.finish();
                        return true;
                    case R.id.menu_item_go_to_location:
                        actionMode.finish();
                        MapFragment mapFragment = MapFragment.newInstance(getActivity(), l.getLocationId());
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                mapFragment, MainActivity.MAP_FRAGMENT_TAG).commit();
                        ((NavigationView)getActivity().findViewById(R.id.nav_view)).setCheckedItem(R.id.nav_map);
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                //
            }
        });
        return v;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Lesson lesson = ((LessonAdapter) getListAdapter()).getItem(position);
        editLesson(lesson);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((LessonAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_lesson_list, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem toggleItem = menu.findItem(R.id.menu_item_toggle_alarm);
        if (LessonAlarms.isAlarmActive(getActivity())) {
            toggleItem.setTitle(R.string.stop_alarm);
        } else {
            toggleItem.setTitle(R.string.start_alarm);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_lesson:
                newLesson();
                return true;
            case R.id.menu_item_toggle_alarm:
                boolean shouldStartAlarm = !LessonAlarms.isAlarmActive(getActivity());
                LessonAlarms.setAlarms(getActivity(), shouldStartAlarm);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    getActivity().invalidateOptionsMenu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LessonLab.get(getActivity()).saveLessons();
    }

    private void newLesson() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ScheduleEditorFragment dialog = new ScheduleEditorFragment();
        dialog.setTargetFragment(ScheduleFragment.this, REQUEST_ADD_LESSON);
        dialog.setArguments(new Bundle());
        dialog.show(fm, DIALOG_ADD_LESSON);
    }

    public void editLesson(Lesson lesson) {

        UUID id = lesson.getId();
        int locationId = lesson.getLocationId();
        String title = lesson.getTitle();
        int hour = lesson.getHour();
        int minute = lesson.getMinute();
        int days = lesson.getClassDaysAsInt( lesson.getClassDays() );

        FragmentManager fm = getActivity().getSupportFragmentManager();
        ScheduleEditorFragment dialog = ScheduleEditorFragment.newInstance(id, locationId, title, hour, minute, days);
        dialog.setTargetFragment(ScheduleFragment.this, REQUEST_EDIT_LESSON);
        dialog.show(fm, DIALOG_EDIT_LESSON);
    }

    public void updateUI() {
        if (LessonLab.get(getContext()).getCount() > 0) {
            mListContainer.setVisibility(View.VISIBLE);
        } else {
            mListContainer.setVisibility(View.GONE);
        }
        ((LessonAdapter) getListAdapter()).notifyDataSetChanged();
    }

    private class LessonAdapter extends ArrayAdapter<Lesson> {

        public LessonAdapter(ArrayList<Lesson> lessons) {
            super(getActivity(), 0, lessons);
        }

        @Override
        public View getView(int position, View convertView, final ViewGroup parent) {

            //If we weren't given a view, create one
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_lessons, null);
            }

            //Configure view for the Lesson
            Lesson l = getItem(position);
            TextView titleTextView = (TextView) convertView.findViewById(R.id.lesson_list_item_titleTextView);
            titleTextView.setText(l.getTitle());

            TextView classroomTextView = (TextView) convertView.findViewById(R.id.lesson_list_item_locationTextView);
            classroomTextView.setText(LocationTree.get(getContext()).findById(l.getLocationId()).getTitle());

            TextView timeTextView = (TextView) convertView.findViewById(R.id.lesson_list_item_timeTextView);
            timeTextView.setText(l.getFormatedTime());

            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            RecyclerView daysList = convertView.findViewById(R.id.days_not_checkable_list);
            daysList.setLayoutManager(layoutManager);
            daysList.setHasFixedSize(true);
            String[] dayOptions = {"D", "L", "M", "M", "J", "V", "S"};
            final DaysAdapter daysAdapter = new DaysAdapter(getContext(), dayOptions, l.getClassDays(), R.layout.list_item_days);
            daysList.setAdapter(daysAdapter);

            return convertView;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        updateUI();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean isOn = prefs.getBoolean(LessonAlarms.PREF_IS_ALARM_ON, false);
        LessonAlarms.setAlarms(getContext(), isOn);
    }
}
