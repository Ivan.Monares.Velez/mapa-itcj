package com.example.mapaitcj;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

public class LessonLab {

    private static final String TAG = "LessonLab";
    private static final String FILENAME = "lessons.json";
    private ArrayList<Lesson> mLessons;

    private static LessonLab sLessonLab;
    private JSONLessonSerializer mSerializer;

    private LessonLab(Context context) {

        mSerializer = new JSONLessonSerializer(context, FILENAME);
        LocationTree lt = LocationTree.get(context);

        try{
            mLessons = mSerializer.loadLessons();
            Log.d(TAG, "Loaded lessons successfully");
        } catch (Exception e){
            mLessons = new ArrayList<Lesson>();
            Log.e(TAG, "Error while loading lessons", e);
        }
    }

    public static LessonLab get(Context c) {
        if (sLessonLab == null) {
            sLessonLab = new LessonLab(c.getApplicationContext());
        }
        return  sLessonLab;
    }

    public ArrayList<Lesson> getLessons() {
        return mLessons;
    }

    public Lesson getLesson(UUID id) {
        for (Lesson l : mLessons) {
            if (l.getId().equals(id))
                return l;
        }
        return null;
    }

    public void addLesson(Lesson lesson){
        mLessons.add(lesson);
    }

    public void deleteLesson(Lesson lesson){
        mLessons.remove(lesson);
    }

    public int getCount(){
        return mLessons.size();
    }

    public boolean saveLessons(){
        try{
            mSerializer.saveLessons(mLessons);
            Log.d(TAG, "lessons saved to file");
            return true;
        }catch (Exception e){
            Log.e(TAG, "Error saving lessons: ", e);
            return false;
        }
    }
}
