package com.example.mapaitcj;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

public class LessonListItemLayout extends RelativeLayout {

    public LessonListItemLayout(Context context) {
        this(context, null);
    }
    public LessonListItemLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true;
    }
}
